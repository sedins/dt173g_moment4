# DT173G - MOMENT 4
___

## Beskrivning

Detta TypeScript läser ut de tio mest förekommande orden som finns i första kapitlet av boken "The hitchhikers guide to the galaxy".

## Installation

Använd följande kommando i ett kommandofönster för att klona projektet till lokal katalog
`git clone https://sedins@bitbucket.org/sedins/dt173g_moment4.git`

## Kommandon

**tsc hitch.ts** - Kompilerar TypeScript till JavaScript

**node hitch.js** - Kör JavaScript från kommandoprompt
___

*Stefan Hälldahl, 2018-09-24*