var fs = require('fs');
fs.readFile("hitch.txt", 'utf8', function (err, data) {
    if (err)
        throw err;
    var reg = /[ \t\n\r]+/g; // Reg exp declaration to strip tab, newline, carriage return and empties
    var clean = data.split(reg); // Split file content with regular expression to get array
    var count = {};
    for (var _i = 0, clean_1 = clean; _i < clean_1.length; _i++) {
        var i = clean_1[_i];
        count[i] = (count[i] || 0) + 1; // Loop all words and count them
    }
    var sorted = [];
    for (var key in count)
        sorted.push([key, count[key]]); // Create a new array for use - sorted[word] = count;
    sorted.sort(function (a, b) { return a[1] - b[1]; }); // Sort the array based on the second element (count)
    sorted.reverse(); // Reverse the result array so that most common words comes first
    console.log(sorted.slice(0, 10)); // Print first ten rows to the console
});
