declare function require(name:string);                     // Make it possible to use require in TypeScript

var fs = require('fs');

fs.readFile("hitch.txt", 'utf8', function(err, data) {     // Read all of the file content 
    if (err) throw err;

    let reg:RegExp = /[ \t\n\r]+/g;                        // Reg exp declaration to strip tab, newline, carriage return and empties
    let clean = data.split(reg);                           // Split file content with regular expression to get array
    let count = {};

    for (let i of clean) {
        count[i] = (count[i]||0) + 1;                      // Loop all words and count them
    }

    var sorted = [];
    for (let key in count) sorted.push([key, count[key]]); // Create a new array for use - sorted[word] = count;
    sorted.sort(function (a, b) { return a[1] - b[1] });   // Sort the array based on the second element (count)
    sorted.reverse();                                      // Reverse the result array so that most common words comes first

    console.log(sorted.slice(0, 10));                      // Print first ten rows to the console
});
